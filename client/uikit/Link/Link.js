import Link from 'next/link';

export const NextLink = ({ href, text, linkProps, ...rest }) => (
  <Link href={href} {...linkProps}>
    <a {...rest}>{text}</a>
  </Link>
);
