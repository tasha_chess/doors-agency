export const navLinks = [
  {
    href: '/',
    text: 'Главная'
  },
  {
    href: '/real-estate',
    text: 'Наши объекты'
  },
  {
    href: '/rules',
    text: 'Шесть правил'
  },
  {
    href: '/faq',
    text: 'Вопросы и ответы'
  },
  {
    href: '/reviews',
    text: 'Отзывы'
  },
  {
    href: '/contacts',
    text: 'Контакты'
  },
]
