import { MainLayout } from "../../components";

const RealEstate = () => {
  return (
    <MainLayout>
      <h1>Real Estate</h1>
    </MainLayout>
  );
};

export default RealEstate;
