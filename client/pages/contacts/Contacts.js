import { MainLayout } from "../../components";

const Contacts = () => {
  return (
    <MainLayout title="Contacts">
      <h1>Contacts</h1>
    </MainLayout>
  );
};

export default Contacts;
