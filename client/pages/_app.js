import Head from 'next/head';
import '../styles/global.scss';
import { MainLayout } from "../components";
import 'antd/dist/antd.css';

export default function App({ Component, pageProps}) {
  return (
  <div>
    <Head>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Component {...pageProps} />
  </div>
  );
};
