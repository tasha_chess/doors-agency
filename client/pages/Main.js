import { MainLayout } from "../components";
import styles from './Main.module.scss';

import { list } from '/staticData/main';

const Main = () => {
  return (
    <MainLayout title="Home">
      <div className={styles.imgBg}>
        <div className={styles.aboutUs}>
          <p className={styles.aboutUs__title}>О нас</p>
          <p className={styles.aboutUs__desc}><b>DOORS AGENCY</b> - это команда молодых специалистов с богатым опытом работы и отличным знанием  рынка недвижимости. Мы занимаемся продажей и арендой жилой и коммерческой недвижимости, а также доверительным управлением. Наша компания - ваше универсальное решение для любых нужд. </p>
        </div>
      </div>
      <div className={styles.why}>
        <p className={styles.why__title}>Почему с агентом лучше, чем без?</p>
        <ol className={styles.why__list}>
          {list.map(({ title, text }, idx) => (<li key={idx}>
            <p className={styles.why__list__title}>{title}</p>
            <p className={styles.why__list__desc}>{text}</p>
          </li>))}
        </ol>
      </div>
    </MainLayout>
  );
};

export default Main;
