import { useState } from 'react';
import { NextLink } from '/uikit';
import Link from 'next/link';
import { MenuOutlined, CloseOutlined } from '@ant-design/icons';

import styles from './Menu.module.scss';
import { navLinks } from "/staticData/menus";
import Logo from '/images/logo.svg';

export const Menu = () => {
  const [isVisible, setVisible] = useState(false);
  return (
    <>
      <div className={styles.menu}>
        <div>{navLinks.slice(0, 3).map((link) => <NextLink className={styles.menuItem} key={link.href} {...link} /> )}</div>
        <Link href="/pages"><a><Logo className={styles.logo} /></a></Link>
        <div>{navLinks.slice(3).map((link) => <NextLink className={styles.menuItem} key={link.href} {...link} /> )}</div>
      </div>
        <div className={styles.mobileMenu} style={{ opacity: isVisible ? '1' : '0'}}>
         {navLinks.map((link) => <NextLink className={styles.menuItem} key={link.href} {...link} /> )}
          <CloseOutlined className={styles.closeBtn} onClick={() => setVisible(false)} />
        </div>
      {!isVisible && <MenuOutlined className={styles.openBtn} onClick={() => setVisible(true)} />}
    </>
  );
};
