import { Menu } from '/components';
import Head from 'next/head';
import { Header } from './Header';
import styles from './MainLayout.module.scss'

export const MainLayout = ({ children, title }) => {
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <div>
        <Header>
          <Menu />
        </Header>
        <div className={styles.container}>
          {children}
        </div>
      </div>
   </>
  );
};
