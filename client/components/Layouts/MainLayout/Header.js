import Link from 'next/link';
import styles from './Header.module.scss';
import Logo from '/images/logo.svg';
import { Menu } from '/components';

export const Header = () => {
  return (
    <div className={styles.header}>
      <Link href="/pages"><a><Logo className={styles.logo} /></a></Link>
      <Menu />
    </div>
  );
}
